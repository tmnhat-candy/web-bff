package com.candy.applicant.dto;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.experimental.FieldDefaults;

import java.util.List;

@Getter
@Builder(toBuilder = true)
@FieldDefaults(level = AccessLevel.PRIVATE)
public class ApplicantCombineDTO {

    Integer id;
    String name;
    List<AppointmentDTO> appointments;
}

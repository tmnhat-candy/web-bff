package com.candy.applicant.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;

@FieldDefaults(makeFinal = true)
@RequiredArgsConstructor
@Getter
public enum CandyServiceEnum {

    APPOINTMENT_SERVICE("http://appointment-service"),
    APPLICANT_SERVICE("http://applicant-service");

    String serviceId;
}

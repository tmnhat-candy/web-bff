package com.candy.applicant.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;

@FieldDefaults(makeFinal = true)
@RequiredArgsConstructor
@Getter
public enum ApplicantServiceEnum {

    GET_APPLICANT("/api/v1/applicant/reactive/get");

    String path;
}

package com.candy.applicant.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;

@FieldDefaults(makeFinal = true)
@RequiredArgsConstructor
@Getter
public enum AppointmentServiceEnum {

    QUERY_APPOINTMENT("/api/v1/appointment/reactive/query");

    String path;
}

package com.candy.applicant.repo.entity;

import lombok.Builder;
import lombok.Getter;

@Builder(toBuilder = true)
@Getter
public class ApplicantEntity {

    Integer id;
    String name;
}

package com.candy.applicant.controller;

import com.candy.applicant.common.dto.TransactionResponse;
import lombok.experimental.UtilityClass;

@UtilityClass
public class BaseService {

    public <T> TransactionResponse<T> buildOnSuccess(T object) {
        return TransactionResponse.<T>builder()
                .code("2xx")
                .msg("success")
                .body(object)
                .build();
    }
}

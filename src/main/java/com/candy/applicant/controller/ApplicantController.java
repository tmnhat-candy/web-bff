package com.candy.applicant.controller;

import com.candy.applicant.common.dto.TransactionResponse;
import com.candy.applicant.dto.ApplicantCombineDTO;
import com.candy.applicant.service.CombineService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/api/v1/bff")
public class ApplicantController {

    @Autowired
    CombineService combineService;

    @GetMapping("/combine")
    public Mono<TransactionResponse<ApplicantCombineDTO>> combine(@RequestParam("applicantId") String applicantId) {
        return combineService.combine(applicantId)
                .map(BaseService::buildOnSuccess)
                .onErrorResume(Mono::error);
    }
}

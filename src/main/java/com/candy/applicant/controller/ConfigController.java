package com.candy.applicant.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/config")
@RefreshScope
public class ConfigController {

    @Value("${spring.application.name}")
    String configName;

    @Value("${candy.service.instance}")
    String serviceInstance;

    @GetMapping("/name")
    public ResponseEntity<String> getConfigName() {
        return ResponseEntity.ok(configName);
    }

    @GetMapping("/service-instance")
    public ResponseEntity<String> getServiceInstance() {
        return ResponseEntity.ok(serviceInstance);
    }
}

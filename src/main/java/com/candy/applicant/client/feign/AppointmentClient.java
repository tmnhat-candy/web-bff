package com.candy.applicant.client.feign;

import com.candy.applicant.dto.AppointmentDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@FeignClient(name = "appointment-service")
@RequestMapping("/api/v1/appointment")
public interface AppointmentClient {

    @GetMapping("/query")
    ResponseEntity<AppointmentDTO> queryAppointments();
}

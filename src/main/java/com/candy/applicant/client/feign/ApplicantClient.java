package com.candy.applicant.client.feign;

import com.candy.applicant.dto.ApplicantCombineDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@FeignClient(name = "applicant-service")
@RequestMapping("/api/v1/applicant")
public interface ApplicantClient {

    @GetMapping("/get")
    ResponseEntity<ApplicantCombineDTO> getApplicant();
}

package com.candy.applicant.client.reactive;

import com.candy.applicant.config.CandyWebClient;
import com.candy.applicant.dto.AppointmentDTO;
import com.candy.applicant.enums.AppointmentServiceEnum;
import com.candy.applicant.enums.CandyServiceEnum;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;

@Service
@Slf4j
public class RxAppointmentClient {

    @Autowired
    CandyWebClient webClient;

    public Flux<AppointmentDTO> queryAppointments() {
        return webClient
                .build(CandyServiceEnum.APPOINTMENT_SERVICE.getServiceId())
                .execute(HttpMethod.GET, AppointmentServiceEnum.QUERY_APPOINTMENT.getPath())
                .bodyToFlux(AppointmentDTO.class);
    }
}

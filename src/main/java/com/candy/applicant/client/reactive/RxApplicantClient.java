package com.candy.applicant.client.reactive;

import com.candy.applicant.config.CandyWebClient;
import com.candy.applicant.dto.ApplicantCombineDTO;
import com.candy.applicant.dto.ApplicantDTO;
import com.candy.applicant.enums.ApplicantServiceEnum;
import com.candy.applicant.enums.CandyServiceEnum;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
@Slf4j
public class RxApplicantClient {

    @Autowired
    CandyWebClient webClient;

    public Mono<ApplicantDTO> getApplicant() {
        return webClient
                .build(CandyServiceEnum.APPLICANT_SERVICE.getServiceId())
                .execute(HttpMethod.GET, ApplicantServiceEnum.GET_APPLICANT.getPath())
                .bodyToMono(ApplicantDTO.class);
    }
}

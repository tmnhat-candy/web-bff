package com.candy.applicant.common.converter;

import com.candy.applicant.dto.ApplicantDTO;
import com.candy.applicant.repo.entity.ApplicantEntity;
import org.springframework.stereotype.Service;

@Service
public class ApplicantConverter implements BaseConverter<ApplicantDTO, ApplicantEntity> {

    @Override
    public ApplicantDTO toDto(ApplicantEntity entity) {
        return ApplicantDTO.builder()
                .id(entity.getId())
                .name(entity.getName())
                .build();
    }

    @Override
    public ApplicantEntity toEntity(ApplicantDTO dto) {
        return ApplicantEntity.builder()
                .id(dto.getId())
                .name(dto.getName())
                .build();
    }
}

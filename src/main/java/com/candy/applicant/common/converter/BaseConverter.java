package com.candy.applicant.common.converter;

public interface BaseConverter<D, E> {

    D toDto(E entity);

    E toEntity(D dto);
}

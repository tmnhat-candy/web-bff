package com.candy.applicant.common.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.extern.jackson.Jacksonized;

@Builder(toBuilder = true)
@Jacksonized
@Getter
public class TransactionResponse<T> {
    String code;
    String msg;
    T body;
}

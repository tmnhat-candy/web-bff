package com.candy.applicant;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class WebBFFApplication {

	public static void main(String[] args) {
		SpringApplication.run(WebBFFApplication.class, args);
	}

}

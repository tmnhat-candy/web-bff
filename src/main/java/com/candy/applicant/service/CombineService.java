package com.candy.applicant.service;

import com.candy.applicant.client.reactive.RxApplicantClient;
import com.candy.applicant.client.reactive.RxAppointmentClient;
import com.candy.applicant.dto.ApplicantCombineDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
@Slf4j
public class CombineService {

    @Autowired
    RxApplicantClient applicantClient;

    @Autowired
    RxAppointmentClient appointmentClient;

    public Mono<ApplicantCombineDTO> combine(String id) {
        log.info("Combine function example with reactive program, id = {}", id);

        Mono<ApplicantCombineDTO> m1 = appointmentClient.queryAppointments()
                .collectList()
                .map(appointments ->
                        ApplicantCombineDTO.builder().appointments(appointments).build()
                );

        Mono<ApplicantCombineDTO> m2 = applicantClient.getApplicant().map(applicant -> ApplicantCombineDTO.builder()
                .id(applicant.getId())
                .name(applicant.getName())
                .build());

        return Mono.zip(m1, m2, (one, two) ->
                ApplicantCombineDTO.builder()
                        .id(two.getId())
                        .name(two.getName())
                        .appointments(one.getAppointments())
                        .build()
        );
    }
}
